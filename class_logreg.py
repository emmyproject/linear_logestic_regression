#-------------------------------------------------------------------------
#Logistic regression is a linear classifier.
#Here a linear function 𝐴(𝐱)=𝑏+𝑤𝑥, which is also called the logit is used.
#The variables 𝑏 and w are the estimators of the regression coefficients,
#which are also called the predicted weights or just coefficients.
#The process of calculating the best weights using available observations
#is called model training or fitting
#Created by EM on 31/8/2021
#------------------------------------------------------------------------- 
import timeit
import numpy as np
import copy
import scipy



class log_reg():
       
        
    def sigmoid(self,x):
        """
        Compute the sigmoid of x

        Arguments:
        x -- A scalar or numpy array of any size.

        Return:
        s -- sigmoid(x)=1/(1+exp(-x))
        """
        s=1./(1.+np.exp(-x))
        return s

    def init_zeros(self,d):
        """
        This function creates a vector of zeros of shape (dim, 1) for w
        and initializes b to 0.
        
        Argument:
        d -- size of the w vector we want (or number of parameters in this case)
    
        Returns:
        w -- initialised vector of shape (dim, 1)
        b -- initialised scalar (corresponds to the bias) of type float
        """
        w=np.zeros((d,1))
        b=0.

        return w, b

    
    def propagate(self,w, b,X, Y):
        """
        Implement the cost function and its gradient for the propagation 

        Arguments:
        w -- weights, a numpy array of size (num_px * num_px * 3, 1)
        b -- bias, a scalar
        X -- data of size (num_px * num_px * 3, number of examples)
        Y -- true "label" vector (containing 0 if non-cat, 1 if cat) of size (1, number of examples)

        Return:
        cost -- negative log-likelihood cost for logistic regression
            =J=-\sum_i=1^m (y^i.log(a^i))+(1-y^i).log(1-a^i)/m
        dw -- gradient of the loss with respect to w, thus same shape as w
            =dJ/dw=X(A-Y)^T/m
        db -- gradient of the loss with respect to b, thus same shape as b
            =dJ/db=\sum_i=1^m (a^i-y^i)/m
        """
    
        m = X.shape[1]
        A = self.sigmoid(np.dot(w.T,X)+b)
        cost=-np.sum(np.dot(Y,np.log(A).T)+np.dot((1.-Y),np.log(1.-A).T))/m

        # BACKWARD PROPAGATION (TO FIND GRAD)
        dw=np.dot(X,(A-Y).T)/m
        db=np.sum(A-Y)/m
        cost = np.squeeze(np.array(cost))
  
        grads = {"dw": dw,
                 "db": db}
    
        return grads, cost
    
    
    def optimize(self,w, b,X,Y, num_iterations=100,
                 learning_rate=0.009, print_cost=False):
        """
        This function optimizes w and b by running a gradient descent algorithm
        of minimizing the cost function 𝐽
    
        Arguments:
        w -- weights, a numpy array of size (num_px * num_px * 3, 1)
        b -- bias, a scalar
        num_iterations -- number of iterations of the optimization loop
        learning_rate -- learning rate of the gradient descent update rule
        print_cost -- True to print the loss every 100 steps
    
        Returns:
        params -- dictionary containing the weights w and bias b
        grads -- dictionary containing the gradients of the weights and bias with respect to the cost function
        costs -- list of all the costs computed during the optimization, this will be used to plot the learning curve.
        """
    
        w = copy.deepcopy(w)
        b = copy.deepcopy(b)
    
        costs = []
        for i in range(num_iterations):
            grads, cost = self.propagate(w, b,X,Y)   
                    
            # Retrieve derivatives from grads
            dw = grads["dw"]
            db = grads["db"]
        
            # update parameters
            w=w-learning_rate*dw
            b=b-learning_rate*db
        
            # Record the costs
            if i % 100 == 0:
                costs.append(cost)
        
                # Print the cost every 100 training iterations
                if print_cost:
                    print ("Cost after iteration %i: %f" %(i, cost))
    
        params = {"w": w,
                  "b": b}
    
        grads = {"dw": dw,
                 "db": db}
    
        return params, grads, costs
    
    
    def predict(self,w, b,X):
        '''
        Predict whether the label is 0 or 1 using 
        learned logistic regression parameters (w, b)
    
        Arguments:
        w -- weights, a numpy array of size (num_px * num_px * 3, 1)
        b -- bias, a scalar
    
        Returns:
        Y_prediction -- a numpy array (vector) containing all predictions (0/1)
        for the examples in X
        '''
    
        m = X.shape[1]
        Y_prediction = np.zeros((1, m))
        w = w.reshape(X.shape[0], 1)
    
        # Compute vector "A" predicting the probabilities of a cat being present in the picture
        A=self.sigmoid(np.dot(w.T,X)+b)
        
        for i in range(A.shape[1]):
            # Convert probabilities A[0,i] to actual predictions p[0,i]
            if A[0, i] > 0.5 :
                Y_prediction[0,i] =1 
            else:
                Y_prediction[0,i] =0        
    
        return Y_prediction
    
    def model_code(self,X_train, Y_train,X_test, Y_test,
              num_iterations=2000, learning_rate=0.5, print_cost=False):
        """
        Builds the logistic regression model
    
        Arguments:
        self.dataX  -- training set
        sself.dataY -- training labels 
        X_test -- test set represented by a numpy array of shape
        (num_px * num_px * 3, m_test)
        Y_test -- test labels represented by a numpy array (vector) of shape (1, m_test)
        num_iterations -- hyperparameter representing the number of iterations
        to optimize the parameters
        learning_rate -- hyperparameter representing the learning rate used 
        in the update rule of optimize()
        print_cost -- Set to True to print the cost every 100 iterations
    
        Returns:
        d -- dictionary containing information about the model.
        """
        start = timeit.default_timer()
        w, b = self.init_zeros(X_train.shape[0])#   np.zeros((X_train.shape[0],1)), 0.0
        params, grads, costs = self.optimize(w, b,X_train,Y_train,
                                             num_iterations, learning_rate, print_cost)
        w = params["w"]
        b = params["b"]
        Y_prediction_test = self.predict(w,b, X_test) 
        Y_prediction_train = self.predict(w,b, X_train)

        # Print train/test Errors
        print ('\nUsing the code')
        print ('----------------------------------------------')
        print ("\nTrain accuracy of logistic regression: {} %"
               .format(100 - np.mean(np.abs(Y_prediction_train - Y_train)) * 100))
        print ("Test accuracy of logistic regression: {} %"
               .format(100 - np.mean(np.abs(Y_prediction_test - Y_test)) * 100))
            
        stop = timeit.default_timer()
        calc_time = stop - start
        d = {"costs": costs,
             "Y_prediction_test": Y_prediction_test, 
             "Y_prediction_train" : Y_prediction_train, 
             "w" : w, 
             "b" : b,
             "learning_rate" : learning_rate,
             "num_iterations": num_iterations,
             "calc_time" : calc_time
            }

        
        return d
