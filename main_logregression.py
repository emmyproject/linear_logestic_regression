#--------------------------------------------------------------------
#EM 30/8/2021
#A logestic linear regression program
#to recognise cat and non-cat images
#test the code by the similar method in sklearn library
#To get more printed information please set info to True
#Plots confusion matrix
#Plots the misclassified images of test set 
#--------------------------------------------------------------------
import matplotlib.pyplot as plt
import class_data as dt
import numpy as np
import matplotlib.pyplot as plt
from class_color import bcolors as bc

data=dt.dataset()
train_x, train_y,test_x, test_y,classes=data.pic_prprtion(print_info=False)
#Feel free to change the index and lrate
num_px=64
index=6

print (bc.BOLD+"\n Logistic linear regression classifier")
print (" to recognise cat and non-cat pictures")
print ("========================================="+bc.ENDC)

logistic_code,logistic_pylib=data.models(train_x, train_y,test_x,
                                         test_y,num_iter=2000,\
                                         lrate=0.005,info=True)


#Test the linear regression model for the test set of data
print (bc.BOLD+"\nTesting the classifier model using")
print ("the code and python library for the test set index "+str(index))
print ("****************************************************"+bc.ENDC)

data.print_state(np.squeeze(logistic_pylib["Y_prediction_test"][index])
                 ,", python library predicts a \"",
                 classes)
data.print_state(np.squeeze(logistic_code["Y_prediction_test"][0,index])
                 ,", logestic regression code predicts a \"",
                 classes)

plt.imshow(test_x[:,index].reshape(num_px,num_px,3))
plt.title('Index '+str(index)+' of Test set ')

#Plots confusion matrix
print (bc.BOLD+"\nPlotting the confusion matrix ..."+bc.ENDC)
cm = logistic_pylib['confusion matrix']

fig, ax = plt.subplots(figsize=(8, 8))
ax.set_title('Confusion matrix',fontsize=20)
ax.imshow(cm)
ax.grid(False)
ax.xaxis.set(ticks=(0, 1), ticklabels=('Predicted 0s', 'Predicted 1s'))
ax.yaxis.set(ticks=(0, 1), ticklabels=('Actual 0s', 'Actual 1s'))
ax.set_ylim(1.5, -0.5)
for i in range(2):
    for j in range(2):
        ax.text(j, i, cm[i, j], ha='center', va='center', color='red')

#Finding the misclassified images' index
print (bc.BOLD+"\nPlotting misclassified images from the test set"+bc.ENDC)
missclassified=np.squeeze(np.where(np.squeeze(test_y) != \
                                   np.squeeze(\
                                       logistic_code["Y_prediction_test"])) 
                         )
n=len(missclassified)
m=int(n/5)+ n%5

plt.figure(figsize=(15,10))
for index in enumerate(missclassified):
    plt.subplot(m, 5, index[0] + 1)
    plt.imshow(test_x[:,index[1]].reshape(num_px,num_px,3), cmap=plt.cm.gray)
    plt.title('index: {}, Predicted: {}, Actual: {}'
              .format(index[1],
                  classes[int(logistic_code["Y_prediction_test"]\
                              [0,index[1]])].decode("utf-8"),
                  classes[int(test_y[0,index[1]])].decode("utf-8")),
              fontsize = 8
             )
plt.suptitle('Misclassified images in the test set',fontsize=20)
plt.show()

print ("\nLinear regression program ends.\n")



