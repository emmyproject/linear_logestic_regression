#---------------------------------------------------------------------
#Linear logestic regression
#using sklearn library
#Created by EM on 31/8/2021
#---------------------------------------------------------------------

import timeit
import numpy as np
import sklearn
import sklearn.linear_model
from sklearn.metrics import classification_report, confusion_matrix

class python_lib():
    
    def model_pylib(self,X_train, Y_train,X_test, Y_test,iter_max=2000):
        """
        Train the logistic regression classifier
        """
        start = timeit.default_timer()
        clf = sklearn.linear_model.LogisticRegression\
        (solver = 'liblinear', max_iter=iter_max,random_state=0);
        clf.fit(X_train.T, np.squeeze(Y_train.T));
        b=clf.intercept_
        w=clf.coef_
        sigmoid=clf.predict_proba(X_test.T)
        test_predict = clf.predict(X_test.T)
        train_predict = clf.predict(X_train.T)
        score = clf.score(X_test.T, Y_test.T)
        confus_matrix=confusion_matrix(Y_test.T, test_predict)
        print ('\nUsing the python library')
        print ('---------------------------------------------')
        print ('Train accuracy of logistic regression: %d '
               % float(100 - np.mean(np.abs(train_predict - Y_train)) * 100)+\
               '% ')
        print ('Test accuracy of logistic regression: %d '
               % float(100 - np.mean(np.abs(test_predict - Y_test)) * 100)+\
               '% ')
        print("score= ", score)
            
        stop = timeit.default_timer()
        calc_time = stop - start
        d = {"Y_prediction_test": test_predict,
             "Y_prediction_train": train_predict,
             "w" : w, 
             "b" : b,
             "probability" : sigmoid,
             "confusion matrix" : confus_matrix,
             "calc_time" : calc_time
            }
        return d
