#---------------------------------------------------------------------------
#Load cat and non-cat images from python library
#Preprocess loaded images
#Compares the linear logestic regression models by the
#written code and sklearn library
#---------------------------------------------------------------------------
import numpy as np
import h5py
import scipy
from PIL import Image
from scipy import ndimage
import class_logreg as cl
import class_logreg_pylib as clp
import matplotlib.pyplot as plt

class dataset(): 
    
    def __str__(self):
        return bcolors.OKBLUE +f'\nLoading train and test sets\n'+bcolors.ENDC

    
    def load_dataset(self):
        train_dataset = h5py.File('datasets/train_catvnoncat.h5', "r")
        ## train set features
        train_x = np.array(train_dataset["train_set_x"][:])
        # train set labels
        train_y = np.array(train_dataset["train_set_y"][:]) 

        test_dataset = h5py.File('datasets/test_catvnoncat.h5', "r")
        # test set features
        test_x = np.array(test_dataset["test_set_x"][:])
        # test set labels
        test_y = np.array(test_dataset["test_set_y"][:])

        # the list of classes
        classes = np.array(test_dataset["list_classes"][:]) 
    
        train_y = train_y.reshape((1, train_y.shape[0]))
        test_y = test_y.reshape((1, test_y.shape[0]))
    
        return train_x, train_y, test_x, test_y, classes

    
    def pic_prprtion(self,print_info=False):
        train_x, train_y, test_x, test_y, classes = self.load_dataset()
        m_train = train_x.shape[0]
        m_test = test_x.shape[0]
        num_px = train_x.shape[1]
        
        """
        Reshape the training and test examples so that images
        of size (num_px, num_px, 3) are flattened into
        single vectors of shape (num_px ∗ num_px ∗ 3, 1).
        """
        train_x_flatten = train_x.reshape(m_train,-1).T
        test_x_flatten = test_x.reshape(m_test,-1).T

        train_x = train_x_flatten / 255.
        test_x = test_x_flatten / 255.
        #
        if print_info:
            print ("Number of training examples: m_train = " + str(m_train))
            print ("Number of testing examples: m_test = " + str(m_test))
            print ("Height/Width of each image: num_px = " + str(num_px))
            print ("Each image is of size: (" + str(num_px) + ", " +\
                   str(num_px) + ", 3)")
            print ("train_set_x shape: " + str(train_x.shape))
            print ("train_set_y shape: " + str(train_y.shape))
            print ("test_set_x shape: " + str(test_x.shape))
            print ("test_set_y shape: " + str(test_y.shape))
            print ("train_set_x_flatten shape: " + str(train_x_flatten.shape))
            print ("train_set_y shape: " + str(train_y.shape))
            print ("test_set_x_flatten shape: " + str(test_x_flatten.shape))
            print ("test_set_y shape: " + str(test_y.shape)+"\n")

        return train_x, train_y,test_x, test_y,classes
    
    
    def models(self,train_x, train_y,test_x,
                        test_y,num_iter,lrate,info=False):
        mthd_code=cl.log_reg()
        logistic_code=mthd_code.model_code(train_x, train_y,test_x, test_y,\
                    num_iterations=num_iter, learning_rate=lrate, \
                                           print_cost=info)
        print ("Calculation time = ",str(logistic_code["calc_time"])+" s\n")
        
        mthd_pylib=clp.python_lib()
        logistic_pylib=mthd_pylib.model_pylib(train_x, train_y,test_x, test_y,
                                iter_max=num_iter)
        
        print ("Calculation time = ",
               str(logistic_pylib["calc_time"])+" s\n")
        

        return logistic_code, logistic_pylib

    def print_state(self,val,explain_str,classes):
        print ("y = " + str(val) +explain_str +classes[int(val),]\
               .decode("utf-8") +  "\" picture.")
